package com.govinda.entity;

import com.govinda.dao.SingleDaoImpl;
import com.govinda.entity.entities.ColumnName;
import com.govinda.entity.entities.SetValueOf;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public abstract class EntitySerializationDeserialization<T> extends SingleDaoImpl<T> {

    private T callingClass;

    public EntitySerializationDeserialization(String tableName) throws IllegalAccessException, InstantiationException, SQLException {
        super(tableName);
        callingClass = getInstance();
    }

    protected abstract T getInstance();

    //Serialization
    @Override
    public HashMap<String, String> toHashMap(T object) throws InvocationTargetException, IllegalAccessException {

        Method[] methods = object.getClass().getMethods();

        HashMap<String,String> columnsWithValues = new HashMap<>();

        for(Method method:methods){
            ColumnName getColumn = method.getAnnotation(ColumnName.class);
            if(getColumn != null){
                columnsWithValues.put(getColumn.columnName(),String.valueOf(method.invoke(object)));
            }
        }

        return columnsWithValues;
    }

    //deserialization
    @Override
    protected T toEntity(ResultSet resultSet) throws SQLException, InvocationTargetException, IllegalAccessException, ClassNotFoundException, InstantiationException {

        T entity = (T) Class.forName(callingClass.getClass().getName()).newInstance();

        Method[] methods = entity.getClass().getMethods();

        for(Method method:methods){
            SetValueOf setValueOf = method.getAnnotation(SetValueOf.class);
            if(setValueOf != null){
                method.invoke(entity, setValueOf.columnName().equals("id") ? resultSet.getInt(setValueOf.columnName()) : resultSet.getString(setValueOf.columnName()));
            }
        }
        return entity;
    }
}