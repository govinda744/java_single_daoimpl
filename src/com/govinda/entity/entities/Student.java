package com.govinda.entity.entities;

import com.govinda.entity.EntitySerializationDeserialization;

import java.sql.SQLException;

public class Student extends EntitySerializationDeserialization{

    public static final String TABLE_NAME = "student_info";

    private int id;
    private String name;
    private String address;
    private String contact;

    public Student() throws IllegalAccessException, SQLException, InstantiationException {
        super(TABLE_NAME);
    }

    @ColumnName(columnName = "id")
    public int getId() {
        return id;
    }

    @ColumnName(columnName = "name")
    public String getName() {
        return name;
    }

    @ColumnName(columnName = "address")
    public String getAddress() {
        return address;
    }

    @ColumnName(columnName = "contact")
    public String getContact() {
        return contact;
    }

    @SetValueOf(columnName = "id")
    public void setId(int id) {
        this.id = id;
    }

    @SetValueOf(columnName = "name")
    public void setName(String name) {
        this.name = name;
    }

    @SetValueOf(columnName = "address")
    public void setAddress(String address) {
        this.address = address;
    }

    @SetValueOf(columnName = "contact")
    public void setContact(String contact) {
        this.contact = contact;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", contact='" + contact + '\'' +
                '}';
    }

    @Override
    protected Object getInstance() {
        return this;
    }
}