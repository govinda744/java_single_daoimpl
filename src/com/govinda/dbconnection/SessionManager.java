package com.govinda.dbconnection;

import java.sql.SQLException;

public class SessionManager extends Session{


    public SessionManager(String dbName, String user, String password) throws SQLException {
        super(dbName, user, password);
    }

    public Session getOneTimeSession() throws SQLException {
        return getSession();
    }
}
