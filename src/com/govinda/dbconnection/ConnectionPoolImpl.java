package com.govinda.dbconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ConnectionPoolImpl implements ConnectionPool{
    private String dbName;
    private String user;
    private String password;

    private static final int INITIAL_POOL_SIZE = 10;
    private static final int MAX_POOL_SIZE = 20;

    private List<Connection> connectionPool;
    private List<Connection> inUseConnections = new ArrayList<>();

    private ConnectionPoolImpl(String dbName, String user, String password, List<Connection> pool){
        this.dbName = dbName;
        this.user = user;
        this.password = password;
        this.connectionPool = pool;
    }

    public static ConnectionPoolImpl create(String dbName, String user, String password) throws SQLException {

        List<Connection> pool = new ArrayList<>(INITIAL_POOL_SIZE);
        for (int i = 0; i < INITIAL_POOL_SIZE; i++) {
            pool.add(createConnection(dbName,user,password));
        }
        return new ConnectionPoolImpl(dbName,user,password,pool);
    }

    private static Connection createConnection(String dbName,String user,String password) throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://localhost/"+dbName, user, password);
    }

    public void shutDown() throws SQLException {
        this.inUseConnections.forEach(this::releaseConnection);
        for(Connection connection:connectionPool){
            connection.close();
        }
        connectionPool.clear();
    }

    @Override
    public Connection getConnection() throws SQLException {
        if (connectionPool.isEmpty()) {
            if (inUseConnections.size() < MAX_POOL_SIZE) {
                connectionPool.add(createConnection(this.dbName,this.user,this.password));
            } else {
                throw new RuntimeException("Maximum pool size reached, no available connections!");
            }
        }
        Connection connection = connectionPool.remove(connectionPool.size() - 1);
        inUseConnections.add(connection);
        return connection;
    }

    @Override
    public boolean releaseConnection(Connection connection) {
        connectionPool.add(connection);
        return inUseConnections.remove(connection);
    }
}