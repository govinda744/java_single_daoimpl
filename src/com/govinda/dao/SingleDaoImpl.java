package com.govinda.dao;

import com.govinda.dbconnection.Session;
import com.govinda.dbconnection.SessionManager;

import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public abstract class SingleDaoImpl<T> implements SingleDao<T>{

    SessionManager sessionManager;

    private String tableName;

    public SingleDaoImpl(String tableName) throws SQLException {
        this.sessionManager = new SessionManager("school_info","root","password");
        this.tableName = tableName;
    }

    private int setParameterValues(PreparedStatement preparedStatement, Set<Map.Entry<String, String>> entrySet) throws SQLException {
        int parameterIndex = 1;
        for (Map.Entry<String, String> entry : entrySet) {
            if (!entry.getKey().equals("id") && !entry.getValue().isEmpty()) {
                preparedStatement.setString(parameterIndex, entry.getValue());
                parameterIndex++;
            }
        }
        return parameterIndex;
    }

    @Override
    public int add(T entity) throws SQLException, IllegalAccessException, InvocationTargetException {

        StringBuilder columnsName = new StringBuilder();
        StringBuilder columnsValue = new StringBuilder();

        HashMap<String, String> columnsWithValues = toHashMap(entity);

        Set<Map.Entry<String, String>> entrySet = columnsWithValues.entrySet();

        for (Map.Entry<String, String> entry : entrySet) {
            if (!entry.getKey().equals("id") && !entry.getValue().isEmpty()) {
                columnsName.append(entry.getKey()).append(",");
                columnsValue.append("?,");
            }
        }
        columnsName.deleteCharAt(columnsName.toString().length() - 1);
        columnsValue.deleteCharAt(columnsValue.toString().length() - 1);

        String sqlQuery = "INSERT INTO " + tableName + " (" + columnsName + ") VALUES (" + columnsValue + ");";

        String[] returnColumn = {"id"};

        Session session = sessionManager.getOneTimeSession();
        PreparedStatement preparedStatement = session.prepareStatement(sqlQuery,returnColumn);
        setParameterValues(preparedStatement,entrySet);
        ResultSet resultSet = preparedStatement.executeUpdate() != 0?preparedStatement.getGeneratedKeys():null;
        session.closeSession();

        if(resultSet.next()){
            return resultSet.getInt(1);
        } else {
            return 0;
        }
    }

    @Override
    public boolean delete(int id) throws SQLException {

        String sqlQuery = "DELETE FROM " + tableName + " WHERE id = ?";

        Session session = sessionManager.getOneTimeSession();
        PreparedStatement preparedStatement = session.prepareStatement(sqlQuery);
        preparedStatement.setInt(1, id);
        boolean result = preparedStatement.executeUpdate() != 0;
        session.closeSession();

        return result;
    }

    @Override
    public List<T> getAll() throws SQLException, ClassNotFoundException, InvocationTargetException, IllegalAccessException, InstantiationException {
        List<T> resultList = new ArrayList<>();

        String sqlQuery = "SELECT * FROM " + tableName;

        Session session = sessionManager.getOneTimeSession();
        PreparedStatement preparedStatement = session.prepareStatement(sqlQuery);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            resultList.add(toEntity(resultSet));
        }
        session.closeSession();
        return resultList;
    }

    @Override
    public int update(T entity) throws SQLException, IllegalAccessException, InvocationTargetException {
        StringBuilder columns = new StringBuilder();
        HashMap<String, String> entityColumns = toHashMap(entity);

        int id = Integer.parseInt(entityColumns.get("id"));

        Set<Map.Entry<String, String>> entrySet = entityColumns.entrySet();
        for (Map.Entry<String, String> entry : entrySet) {
            if (!entry.getKey().equals("id") && !entry.getValue().isEmpty()) {
                columns.append(entry.getKey()).append("=?,");
            }
        }

        columns = columns.deleteCharAt(columns.toString().length() - 1);
        String sqlQuery = "UPDATE " + tableName + " SET " + columns + " WHERE id = ?";

        Session session = sessionManager.getOneTimeSession();
        PreparedStatement preparedStatement = session.prepareStatement(sqlQuery);

        int  parameterIndex = setParameterValues(preparedStatement,entrySet);
        preparedStatement.setInt(parameterIndex, id);

        int result = preparedStatement.executeUpdate();
        session.closeSession();
        return result != 0 ? id : 0;
    }

    @Override
    public T get(int id) throws SQLException, ClassNotFoundException, InvocationTargetException, IllegalAccessException, InstantiationException {
        T returningEntity = null;
        String sqlQuery = "SELECT * FROM " + tableName + " WHERE id = ?";

        Session session = sessionManager.getOneTimeSession();
        PreparedStatement preparedStatement = session.prepareStatement(sqlQuery);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            returningEntity = toEntity(resultSet);
        }
        session.closeSession();

        return returningEntity;
    }

    @Override
    public int merge(T entity) throws InvocationTargetException, IllegalAccessException, SQLException {

        HashMap<String, String> hashMap = toHashMap(entity);

        int id = Integer.parseInt(hashMap.get("id"));

        String query = "SELECT  * FROM " + tableName + " WHERE id = " + id;

        Session session = sessionManager.getOneTimeSession();
        PreparedStatement preparedStatement = session.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery();

        if (resultSet.next()) {
            session.closeSession();
            throw new RuntimeException("Repeated Primary Key.");

        } else {
            session.closeSession();
            int result = add(entity);
            System.out.println(result);
            return result;
        }
    }

    public abstract HashMap<String, String> toHashMap(T object) throws InvocationTargetException, IllegalAccessException;

    protected abstract T toEntity(ResultSet resultSet) throws SQLException, InvocationTargetException, IllegalAccessException, ClassNotFoundException, InstantiationException;
}
