package com.govinda.dao;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface SingleDao<T> {

    int add(T dbEntity) throws SQLException, IllegalAccessException, InvocationTargetException;
    boolean delete(int id) throws SQLException;
    List<T> getAll() throws SQLException, ClassNotFoundException, InvocationTargetException, IllegalAccessException, InstantiationException;
    int update(T entity) throws SQLException, IllegalAccessException, InvocationTargetException;
    T get(int id) throws SQLException, ClassNotFoundException, InvocationTargetException, IllegalAccessException, InstantiationException;
    int merge(T dbEntity) throws InvocationTargetException, IllegalAccessException, SQLException;

}
