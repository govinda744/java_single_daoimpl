package com.govinda.ui;

import com.govinda.dao.SingleDao;
import com.govinda.entity.entities.Student;
import com.govinda.entity.entities.Teacher;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class MainUI {

    public static void main(String[] args) throws ClassNotFoundException, SQLException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, InstantiationException {

        Scanner userInput = new Scanner(System.in);

        while(true){

            System.out.println("1. Student\n2. Teacher\n3. Exit");
            System.out.println("choose to operate: ");
            int input = userInput.nextInt();
            String choice = input == 1?"Student":"";
            choice += input == 2?"Teacher":"";
            choice += input == 3?"Exit":"";
            switch (choice){

                case "Student":
                    SingleDao studentTableOperator = new Student();
                    while(true){

                        System.out.println("1. Add\n2. Delete\n3. Edit\n4. View all student\n5. Exit");
                        System.out.println("Enter your choice: ");
                        int inputForStudent = userInput.nextInt();
                        String choiceForStudent = inputForStudent==1?"add":"";
                        choiceForStudent += inputForStudent==2?"delete":"";
                        choiceForStudent += inputForStudent==3?"edit":"";
                        choiceForStudent += inputForStudent==4?"viewall":"";
                        choiceForStudent += inputForStudent==5?"exit":"";

                        switch (choiceForStudent){
                            case "add":
                                    Student addStudent = new Student();

                                    System.out.println("Enter name: ");
                                    addStudent.setName(userInput.next());

                                    System.out.println("Enter address: ");
                                    addStudent.setAddress(userInput.next());

                                    System.out.println("Enter contact:");
                                    addStudent.setContact(userInput.next());

                                    int insertedId = studentTableOperator.merge(addStudent);

                                    System.out.println(insertedId != 0 ? "Inserted at id: " + insertedId : "Cannot be Inserted");
                                break;
                            case "delete":

                                System.out.println("Enter the id of student: ");

                                System.out.println(studentTableOperator.delete(userInput.nextInt())?"Dleted.":"Cannot delete");

                                break;
                            case "edit":
                                System.out.println("Enter the id of student: ");

                                int editId = userInput.nextInt();
                                Student editStudent = (Student) studentTableOperator.get(editId);

                                if(editStudent != null){
                                    System.out.println(editStudent);
                                    System.out.println("Enter new name:");
                                    editStudent.setName(userInput.next());
                                    System.out.println("Enter new contact:");
                                    editStudent.setContact(userInput.next());
                                    System.out.println("Enter new address:");
                                    editStudent.setAddress(userInput.next());

                                    int updatedAt = studentTableOperator.update(editStudent);
                                    System.out.println(updatedAt != 0?"Update successful at: "+updatedAt:"Cannot update.");
                                }
                                else{
                                    System.out.println("No entry in the id "+editId);
                                }
                                break;
                            case "viewall":
                                List<Student> students = studentTableOperator.getAll();
                                for (Student showStudent:students) {
                                    System.out.println(showStudent);
                                }
                                break;
                            case "exit":
                                break;
                        }
                        if(choiceForStudent.equals("exit")){
                            break;
                        }
                    }
                    break;
                case "Teacher":
                    SingleDao teacherTableOperator = new Teacher();
                    while(true){

                        System.out.println("1. Add\n2. Delete\n3. Edit\n4. View all teacher\n5. Exit");
                        System.out.println("Enter your choice: ");
                        int inputForTeacher = userInput.nextInt();
                        String choiceForTeacher = inputForTeacher==1?"add":"";
                        choiceForTeacher += inputForTeacher==2?"delete":"";
                        choiceForTeacher += inputForTeacher==3?"edit":"";
                        choiceForTeacher += inputForTeacher==4?"view all":"";
                        choiceForTeacher += inputForTeacher==5?"exit":"";

                        switch (choiceForTeacher){
                            case "add":
                                Teacher addTeacher = new Teacher();

                                System.out.println("Enter name: ");
                                addTeacher.setName(userInput.next());

                                System.out.println("Enter contact: ");
                                addTeacher.setContact(userInput.next());

                                System.out.println("Enter address:");
                                addTeacher.setAddress(userInput.next());

                                int insertedAt = teacherTableOperator.merge(addTeacher);

                                System.out.println(insertedAt != 0 ? "Inserted at id: " + insertedAt : "Cannot be inserted");
                                break;
                            case "delete":

                                System.out.println("Enter the id of teacher: ");

                                System.out.println(teacherTableOperator.delete(userInput.nextInt())?"Deleted.":"Cannot delete");

                                break;
                            case "edit":
                                System.out.println("Enter the id of teacher: ");

                                int editId = userInput.nextInt();

                                Teacher editTeacher = (Teacher) teacherTableOperator.get(editId);

                                if(editTeacher != null) {
                                    System.out.println(editTeacher);
                                    System.out.println("Enter new name:");
                                    editTeacher.setName(userInput.next());
                                    System.out.println("Enter new contact:");
                                    editTeacher.setContact(userInput.next());
                                    System.out.println("Enter new address:");
                                    editTeacher.setAddress(userInput.next());

                                    int updatedAtId = teacherTableOperator.update(editTeacher);

                                    System.out.println(updatedAtId != 0 ? "Update sucessful at id :"+ updatedAtId: "Cannot update.");
                                }
                                else{
                                    System.out.println("No entry in id "+editId);
                                }
                                break;
                            case "view all":
                                List<Teacher> teachers = teacherTableOperator.getAll();
                                for (Teacher showTeacher:teachers) {
                                    System.out.println(showTeacher);
                                }
                                break;
                            case "exit":
                                break;
                        }
                        if(choiceForTeacher.equals("exit")){
                            break;
                        }
                    }
                    break;
                case "Exit":
                    System.exit(0);
                    break;
                default:
                    System.out.println("Enter valid choice.");
            }


        }
    }
}